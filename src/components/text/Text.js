function Textfield({ type, placeholder, value, onChange }) {
  return (
    <input
      type={type}
      placeholder={placeholder}
      value={value}
      onChange={(e) => onChange(e.target.value)}
    ></input>
  );
}

function DropBasic() {
  return (
    <label>
      Food category
      <select name="category">
        <option value="1">Vegetarian</option>
        <option value="2">Halal</option>
        <option value="3">Non-Halal</option>
      </select>
    </label>
  );
}

export { Textfield, DropBasic };
