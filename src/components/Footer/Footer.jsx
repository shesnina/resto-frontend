import React from "react";
import "./footer.css";

export const Footer = () => {
	return (
		<div className="footer d-flex align-items-center justify-content-center mt-5">
			Copyright © 2023. Developed By Team 1
		</div>
	);
};
