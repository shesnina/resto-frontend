import React from "react";
import { Card, Button } from "react-bootstrap";

function CardComponent(restaurants) {
  const { Name, restaurant_address, rating, price_range } = restaurants;
  return (
    <div className="d-flex justify-content-center justify-content-lg-start">
      <Card className="bg-warning-subtle m-2" style={{ width: "20rem" }}>
        {/* <Card.Img variant="top" src={restaurants.Img} /> */}
        <Card.Body>
          <Card.Title>{Name}</Card.Title>
          <Card.Text>Address : {restaurant_address}</Card.Text>
          <Card.Text>Rating : {rating}</Card.Text>
          <Card.Text>Price-range : {price_range}</Card.Text>
          <Button variant="primary">KUY</Button>
        </Card.Body>
      </Card>
    </div>
  );
}

export default CardComponent;
