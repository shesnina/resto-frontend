import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { RouterProvider, createBrowserRouter } from "react-router-dom";
import Home from "../pages/home";

const Routes = () => {
  const routes = [
    {
      path: "/",
      element: <Home />,
    },
  ];
  const router = createBrowserRouter([...routes]);

  return <RouterProvider router={router} />;
};

export default Routes;
