import React from "react";
import ReactDOM from "react-dom";
import App from "./app";

// const root = ReactDOM.render(<App />, document.getElementById(`root`));
// root.render(
//   <React.StrictMode>
//     <App />
//   </React.StrictMode>
// );

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
