import { AuthContext } from "providers/authProvider";
import { useContext } from "react";

const useAuth = () => {
  return useContext(AuthContext);
};

export default useAuth;

// import axios from "axios";

// export default axios.create({
//   baseURL: "http://localhost:4000/api/restaurants",
// });
