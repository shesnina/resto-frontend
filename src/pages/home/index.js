import { React, useState } from "react";
import { Footer } from "../../components/Footer/Footer";
import NavbarNew from "../../components/dropdown/Dropdown";
import { Header } from "../../components/header/Header";
import CardComponent from "../../components/card/CardComponent";
import { Row, Col } from "react-bootstrap";
import useFetch from "../../hooks/useFetch";
import { Textfield, DropBasic } from "../../components/text/Text";

const Home = () => {
  const [price, setPrice] = useState("");
  const { data } = useFetch(`${process.env.REACT_APP_SITE}/allresto`);

  return (
    <div>
      <NavbarNew />
      <Header />
      <div className="d-lg-flex d-xl-flex">
        <div className="w-25">
          <h3 className="text-center">Filter By</h3>
          <h4 className="mt-5">Price</h4>
          <Textfield
            type="price"
            placeholder="price arange 100k - 500k IDR"
            value={price}
            onChange={setPrice}
          ></Textfield>
          <DropBasic />
        </div>
        <Row className="bg-dark-subtle m-1 p-2">
          {data &&
            data.results.map((allrestos, index) => {
              return (
                <Col key={index}>
                  <CardComponent
                    Name={allrestos.restaurant_name}
                    restaurant_address={allrestos.restaurant_address}
                    rating={allrestos.rating}
                    price_range={allrestos.price_range}
                  />
                </Col>
              );
            })}
        </Row>
      </div>
      <Footer />
    </div>
  );
};

export default Home;
